import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_wrapper/connectivity_wrapper.dart';
import 'package:morraipur_flutter/common/httpurl.dart';
import 'package:morraipur_flutter/models/appImagesModal/app_images_modal.dart';
import 'package:morraipur_flutter/screens/homeRoute.dart';
import 'package:morraipur_flutter/models/subSectionModal/sub_section_data_modal.dart';
import 'package:morraipur_flutter/services/remote_services.dart';
import 'package:morraipur_flutter/models/facilitiesModal/facilities_data_modal.dart';
void main() { runApp( MyApp()); }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Mor Raipur',
      home:  MyHomePage(),
      // home: EnterTkc(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  var isInternetAvailable = '1';

  @override
  void initState() {
    getAppImages();
    getSubsectionData();
    getFacilities();
    doesUserExist();
    super.initState();
  }

 @override
  Widget build(BuildContext context) {
    Future.delayed(const Duration(seconds: 2), () {
      try {
        checkIsLogin();
      } catch (e) {
        debugPrint('catch');
      }
    });
    return Scaffold(
      body: Center(
        child: isInternetAvailable == '1'
            ? MainImage()
            : noInternetImageShow(context),
      ),
    );
  }

  Future<void> checkIsLogin() async {
    WidgetsFlutterBinding.ensureInitialized();
    String data ='test';
   if (await ConnectivityWrapper.instance.isConnected) {
      if (!mounted) return;
      setState(() {
        isInternetAvailable = '1';
      });

      if (data != null) {
         Navigator.pushReplacement(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
        );
      } else {
       //Navigate to login page
      }
    } else {
      if (!mounted) return;
      setState(() {
        isInternetAvailable = '0';
      });
    }
  }
  Future<void> doesUserExist() async {
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      FormData formData = FormData.fromMap({
        'mobile_number':'',
        'user_id':'',
        'user_name':'',
        'email_id':'',
      });
      var response = await RemoteServices.getDataModel(context,HttpUrl.checkIfUserExist,formData);
      FacilitiesDataModal modal = FacilitiesDataModal.fromJson(response!);
    } catch (e) {
      print(e);
    }
  }
  Future<void> getFacilities() async{
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      var response = await RemoteServices.getDataModel(context,HttpUrl.getFacilities,FormData());
      FacilitiesDataModal modal = FacilitiesDataModal.fromJson(response!);
    } catch (e) {
      print(e);
    }
  }
  Future<void> getAppImages() async{
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      var response = await RemoteServices.getDataModel(context,HttpUrl.getAppImages,FormData());
      AppImagesModal modal = AppImagesModal.fromJson(response!);
    } catch (e) {
      print(e);
    }
  }

  Future<void> getSubsectionData() async{
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      var response = await RemoteServices.getDataModel(context,HttpUrl.getSubsectionData,FormData());
      SubSectionDataModal modal = SubSectionDataModal.fromJson(response!);
    } catch (e) {
      print(e);
    }
  }
  Widget noInternetImageShow(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('assets/images/wifi.png'),
        const SizedBox(
          height: 10.0,
        ),
      Text('No Internet Connection!..')
      ],
    );
  }

}

class MainImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        const Image(
          image: AssetImage('assets/images/newlook_splashpage_bkg.png'),
          fit: BoxFit.fitHeight,
          height: double.infinity,
          width: double.infinity,
          alignment: Alignment.center,
        ),
      ],
    );
  }
}

