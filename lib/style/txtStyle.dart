import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TxtStyle {
  static TextStyle captionText =
  GoogleFonts.openSans(fontSize: 12,
      fontWeight: FontWeight.w500,color: Colors.black54);
  static TextStyle labelTextStyle =
  GoogleFonts.openSans(fontSize: 12,
      fontWeight: FontWeight.w500,color: Colors.black54);

  static TextStyle smallText =
  GoogleFonts.roboto(fontSize: 13, color: Colors.black);

  static TextStyle headerText = GoogleFonts.abel(
    fontSize: 20,
    fontWeight: FontWeight.w500,
  );

  static TextStyle bigheaderText = GoogleFonts.roboto(
    fontSize: 28,
    color: Colors.grey[600],
    fontWeight: FontWeight.w500,
  );

  static TextStyle bigBrownText = GoogleFonts.roboto(
    fontSize: 20,
    color: Colors.brown[400],
    fontWeight: FontWeight.w400,
 );

  static TextStyle brownText = GoogleFonts.roboto(
    fontSize: 24,
    color: Colors.brown[400],
    fontWeight: FontWeight.w400,
 );
  static TextStyle smallBodyText =
  GoogleFonts.roboto(fontSize: 10, color: Colors.black87,fontWeight: FontWeight.w400,);

  static TextStyle dropDownItemStyle = GoogleFonts.openSans(
    fontSize: 15,
    fontWeight: FontWeight.w500,
    color:Colors.grey[600],
  );
  static TextStyle titleText = GoogleFonts.roboto(
    fontSize: 16,
    fontWeight: FontWeight.w500,
  );

  static TextStyle subtitleText = GoogleFonts.roboto(
    fontSize: 14,
    color: Colors.grey[800],
    fontWeight: FontWeight.w500,
  );
  static TextStyle textFieldStyle = GoogleFonts.roboto(
    fontSize: 16,
    color: Colors.grey[800],
    fontWeight: FontWeight.w500,
  );
  static TextStyle textFieldlabelStyle = GoogleFonts.roboto(
    fontSize: 14,
    color: Colors.grey[600],
    fontWeight: FontWeight.w500,
  );
  static TextStyle subtitleGreyText = GoogleFonts.openSans(
    fontSize: 15,
    color: Colors.grey[700],
    fontWeight: FontWeight.w600,
  );
  static TextStyle titleGreyText = GoogleFonts.openSans(
    fontSize: 22,
    color: Colors.grey[600],
    fontWeight: FontWeight.w600,
  );
  static TextStyle bodyGreyText = GoogleFonts.roboto(
    fontSize: 13,
    color: Colors.grey[600],
    fontWeight: FontWeight.w600,
  );
  static TextStyle smallbodyGreyText = GoogleFonts.roboto(
    fontSize: 12,
    color: Colors.grey[600],
    fontWeight: FontWeight.w500,
  );
  static TextStyle bodyText = GoogleFonts.roboto(
    fontSize: 12,
    color: Colors.grey[700],
    fontWeight: FontWeight.w500,
  );

  static TextStyle whiteBodyText = GoogleFonts.openSans(
    fontSize: 12,
    color: Colors.white,
    fontWeight: FontWeight.w500,
  );

  static TextStyle buttonText = GoogleFonts.roboto(
    fontSize: 14,
    fontWeight: FontWeight.w300,
  );


  static TextStyle customText(Color color,double fontSize,FontWeight weight){
    return GoogleFonts.roboto(
        fontSize: fontSize,
        fontWeight: weight,
        color: color
    );
  }

 static TextStyle buttonTextstyle = GoogleFonts.roboto(
    textStyle: const TextStyle(
        fontSize: 18,
        color: Colors.white,
        fontWeight: FontWeight.w500
    ),
  );

  ButtonStyle bottomButtonStyle = ElevatedButton.styleFrom(
      backgroundColor: Colors.lightBlueAccent
  );

  static TextStyle successText = TextStyle(
      fontWeight: FontWeight.w600, fontSize: 13, color: Colors.green[500]);

  static TextStyle warningText = TextStyle(
      fontWeight: FontWeight.w700, fontSize: 13, color: Colors.yellow[700]);

  static TextStyle dangourText = TextStyle(
      fontWeight: FontWeight.w700, fontSize: 13, color: Colors.red[600]);

  static TextStyle headerBlackText = GoogleFonts.roboto(
    color: Colors.black,
    fontSize: 20,
    fontWeight: FontWeight.w600,
  );


}
