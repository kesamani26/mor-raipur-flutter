
import 'package:cool_alert/cool_alert.dart';
import 'package:flutter/material.dart';

class customDailog{

  static infoAlert(context ,text){
   return CoolAlert.show(
      context: context,
      type: CoolAlertType.info,
      text: 'Buy two, get one free',
    );
  }

  static successAlert(context ,text){
   return CoolAlert.show(
      context: context,
      type: CoolAlertType.success,
      text: 'Transaction completed successfully!',
      autoCloseDuration: Duration(seconds: 2),
    );
  }

  static errorAlert(context,text){
    return CoolAlert.show(
      context: context,
      type: CoolAlertType.error,
      title: 'Oops...',
      text: 'Sorry, something went wrong',
      loopAnimation: false,
    );
  }

  static warningAlert(context,text){
      return  CoolAlert.show(
        context: context,
        type: CoolAlertType.warning,
        text: 'You just broke protocol',
      );
  }
  static confirmAlert(context,text){
    CoolAlert.show(
      context: context,
      type: CoolAlertType.confirm,
      text: 'Do you want to logout',
      confirmBtnText: 'Yes',
      cancelBtnText: 'No',
      confirmBtnColor: Colors.green,
    );
  }

  static loadingAlert(context,){
    CoolAlert.show(
      context: context,
      type: CoolAlertType.loading,
    );
  }

  static customAlert(context,message){

    CoolAlert.show(
      context: context,
      type: CoolAlertType.custom,
      barrierDismissible: true,
      confirmBtnText: 'Close',
      widget: Column(
        mainAxisAlignment: MainAxisAlignment.start,
       children: [
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
         const Text('Property Search Guide'),
       ],
      ),
      onConfirmBtnTap: () async {
        if (message.length < 5) {
          await CoolAlert.show(
            context: context,
            type: CoolAlertType.error,
            text: 'Please input something',
          );
          return;
        }
        Navigator.pop(context);
        await Future.delayed(Duration(milliseconds: 1000));
        await CoolAlert.show(
          context: context,
          type: CoolAlertType.success,
          text: "Phone number '$message' has been saved!.",
        );
      },
    );
  }


}