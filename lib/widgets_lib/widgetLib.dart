import 'package:flutter/material.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import '../common/constants.dart';
import '../style/txtStyle.dart';
import '../common/colors.dart';

Widget buildButton({VoidCallback? onTap, required String text, Color? color}) {
  return Padding(
    padding: const EdgeInsets.only(bottom: 10.0),
    child: MaterialButton(
      color: color,
      minWidth: double.infinity,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
      ),
      onPressed: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: Text(
          text,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    ),
  );
}

Widget buildBrownButton(BuildContext context,
    {VoidCallback? onTap, required String text, Color? color}) {
  return MaterialButton(
    color: Colors.grey[700],
    minWidth: Constants.deviceWidth(context),
    height: 22,
    shape: RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(30.0),
    ),
    onPressed: () {},
    child: Text(text, style: TxtStyle.whiteBodyText),
  );
}

Widget dropDownErrorBuild(BuildContext context, String error, value) {
  return Center(
      child: ListTile(
          title: Icon(
            Icons.wifi_tethering_error,
            size: 30,
            color: AppColors.textColorGreyDark,
          ),
          subtitle: Text(
            'Unable to load data right now',
            style: TxtStyle.headerText,
          )));
}

Widget DropDownItem(text) {
  return Padding(
    padding: const EdgeInsets.only(left: 10, bottom: 3.0),
    child: Expanded(
      child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            text,
            style: TxtStyle.dropDownItemStyle,
          )),
    ),
  );
}

Widget loadingBuild(BuildContext context, String value) {
  return const Center(
    child: CircularProgressIndicator(),
  );
}

ProgressDialog progressDialog(context) {
  ProgressDialog dialog = ProgressDialog(context);
  dialog.style(
      message: 'Please wait...',
      borderRadius: 10.0,
      backgroundColor: Colors.white,
      progressWidget: Container(
        padding: const EdgeInsets.all(12.0),
        child: const CircularProgressIndicator(),
      ),
      elevation: 10.0,
      insetAnimCurve: Curves.easeInOut,
      progress: 0.0,
      maxProgress: 100.0,
      progressTextStyle: const TextStyle(
          color: Colors.black, fontSize: 13.0, fontWeight: FontWeight.w400),
      messageTextStyle: TxtStyle.titleText);
  return dialog;
}

TextFieldProps searchFieldProps(text) {
  return TextFieldProps(
      decoration: InputDecoration(
          hintText: text,
          hintStyle: TxtStyle.titleText,
          icon: const Icon(Icons.search_sharp),
          isDense: true),
      style: TxtStyle.titleText);
}

DropDownDecoratorProps dropDownDecoratorProps(String labeltext, String hint,
    {bool isBorder = true}) {
  return DropDownDecoratorProps(
      dropdownSearchDecoration: InputDecoration(
          hintText: hint,
          hintStyle: TxtStyle.labelTextStyle,
          border: const OutlineInputBorder(
            borderSide:
                BorderSide(width: 3, color: AppColors.primary), //<-- SEE HERE
          ),
          label: Text(
            labeltext,
            style: TxtStyle.textFieldlabelStyle,
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          floatingLabelStyle: TxtStyle.subtitleGreyText));
}

InputDecoration inputDecoration(String labeltext,String hint, Icon icon,){
  return InputDecoration(
    enabledBorder: OutlineInputBorder(),
    hintText: hint,
    hintStyle: TxtStyle.customText(Colors.black54,15,FontWeight.w400),

    contentPadding: const EdgeInsets.symmetric(horizontal: 15,vertical: 17),
    suffixIcon:  icon,
    filled: true,
    labelText: labeltext,
    labelStyle: TxtStyle.customText(AppColors.loginButton,15,FontWeight.w400),
    fillColor: Colors.white,
    focusedBorder: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(4)),
      borderSide: BorderSide(width: 2,color: AppColors.loginButton),
    ),
    border: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.black54)),

  );
}

InputDecoration inputDecorations(String labeltext,String hint,){
  return InputDecoration(
    enabledBorder: OutlineInputBorder(),
    hintText: hint,
    hintStyle: TxtStyle.customText(Colors.black54,15,FontWeight.w400),

    contentPadding: const EdgeInsets.symmetric(horizontal: 15,vertical: 15),
    filled: true,
    labelText: labeltext,
    labelStyle: TxtStyle.customText(AppColors.loginButton,15,FontWeight.w400),
    fillColor: Colors.white,
    focusedBorder: OutlineInputBorder(
      borderRadius: const BorderRadius.all(Radius.circular(4)),
      borderSide: BorderSide(width: 2,color: AppColors.loginButton),
    ),
    border: const OutlineInputBorder(
        borderSide: BorderSide(color: Colors.black54)),

  );
}




Widget dropDownTitle(text) {
  return Column(
    children: [
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Text(text, style: TxtStyle.titleText),
      ),
      const Divider(
        height: 2,
        color: Colors.black45,
      )
    ],
  );
}

Route pushModal(Widget page) {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => page,
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      const begin = Offset(0.0, 1.0);
      const end = Offset.zero;
      const curve = Curves.ease;

      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}

final Widget leading = Builder(
  builder: (BuildContext context) {
    return IconButton(
      icon: const Icon(Icons.arrow_back),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  },
);
