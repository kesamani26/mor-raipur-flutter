class HttpUrl {
  static const String HttpHost_From_Nagar_Nigam = 'http://nagarnigamprojects.in/morraipur/websiervice/webservice/';
  static const String HttpHost = "http://www.smartcity.entitcs.com/webservice/webservice/";

  static const login = HttpHost_From_Nagar_Nigam+'login.php';
  static const login_with_otp = HttpHost_From_Nagar_Nigam + 'login_with_otp.php';
  static const verify_Registration_OTP = HttpHost_From_Nagar_Nigam + "verify_Registration_OTP.php";

  static const getSubsectionData = HttpHost_From_Nagar_Nigam + 'getsubsection.php';
  static const getFacilities = HttpHost_From_Nagar_Nigam + 'getFacilityNameList.php';
  static const getAppImages = HttpHost_From_Nagar_Nigam + 'getAppImageImages.php';
  static const checkIfUserExist = HttpHost_From_Nagar_Nigam + 'checkUserExistOrNot.php';
  static const HttpHost_For_Icon = "http://nagarnigamprojects.in/morraipur/websiervice/webservice/appadmin/image_category/";
  static const get_Ward_Info = HttpHost_From_Nagar_Nigam + 'wardjsson.php';
  static const get_Colony_List = 'https://apis.mcraipur.in/api/payment/locality/list';
  static const get_property_id_from_ddn = HttpHost_From_Nagar_Nigam+"getPropertyDetailsFromDDN.php";

}
