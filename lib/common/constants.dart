import 'package:flutter/material.dart';

import 'package:geolocator/geolocator.dart';
import '../screens/accounts.dart';
import '../style/txtStyle.dart';
import 'colors.dart';



const String currency = '₹';

void showSuccessAlert(BuildContext context,body) async {
  await showDialog(context: (context),
      builder: (builder){
        return AlertDialog(
          icon: Icon(Icons.done,color: Colors.green[200],size: 34,),
          title: Text('Success',style: TxtStyle.headerText,),
          content: Text(body,textAlign:TextAlign.center,style: TxtStyle.subtitleText,),
          actions: [
            ElevatedButton(onPressed: (){
              Navigator.pop(context);
            }, child: Text('Ok',style: TxtStyle.buttonTextstyle,))
          ],
        );
      });
}
void showErrorAlert(BuildContext context,text) async {
  await showDialog(context: (context),
      builder: (builder){
        return AlertDialog(
          icon: Icon(Icons.error_outline_sharp,color: Colors.red[200],size: 34,),
          title: Text('Error',style: TxtStyle.headerText,),
          content: Text(text,textAlign:TextAlign.center,style: TxtStyle.subtitleText,),
          actions: [
            ElevatedButton(onPressed: (){
              Navigator.pop(context);
            }, child: Text('Ok',style: TxtStyle.buttonTextstyle,))
          ],
        );
      });
}


class Constants {
 static double deviceHeight(BuildContext context) => MediaQuery.of(context).size.height;

  static double deviceWidth(BuildContext context) => MediaQuery.of(context).size.width;


  static Future<Position> GetGeoLocationPosition() async {
    bool serviceEnabled;
    LocationPermission permission;
    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      await Geolocator.openLocationSettings();
      return Future.error('Location services are disabled.');
    }
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.

          //fluttertoast package giving beacuse of giving error open ot after solve

        /*Fluttertoast.showToast(
          msg: 'Please Allow Permission',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
        );*/

        return Future.error('Location permissions are denied');
      }
    }
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    }
    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  static AppBar appbar(text) {
    return AppBar(
      backgroundColor: AppColors.colorPrimary,
      title: Text(text, style: TxtStyle.titleText,),
    );
  }
 static AppBar buildAppBar(context,title) {
   return AppBar(
     toolbarHeight: 90,
     elevation: 0,
     title: Column(
       children: [
         Center(child: Text(title!,style: TxtStyle.headerBlackText,)),
         const SizedBox(height: 5,),
         Center(child: Text('raipur municipal corporation',style: TxtStyle.subtitleGreyText,)),
       ],
     ),
     backgroundColor:AppColors.viewBg,
     leading: IconButton(
       onPressed: () {
         Navigator.push(context, MaterialPageRoute(builder: (context)=> UserAccount()));
       },
       icon: const Icon(Icons.account_circle,size: 50,color: Colors.black26,),
     ),
     actions: [
       IconButton(
         onPressed: () {},
         icon: const Icon(Icons.notifications_none_outlined,size: 28,color: Colors.black26,),
       ),
     ],

   );
 }


 static popToast(text){
   /*Fluttertoast.showToast(
     msg: text,
     toastLength: Toast.LENGTH_SHORT,
     gravity: ToastGravity.BOTTOM,
     timeInSecForIosWeb: 1,
   );*/
 }
 static showSuccessAlert(BuildContext context) async {
   await showDialog(context: (context),
       builder: (builder){
         return AlertDialog(
           icon: Icon(Icons.done,color: Colors.green[200],size: 26,),
           title: Text('Success',style: TxtStyle.titleText,),
           content: Text('Expense successfully submitted',textAlign:TextAlign.center,style: TxtStyle.bodyText,),
           actions: [
             TextButton(onPressed: (){
               Navigator.pop(context);
             }, child: Text('Ok',style: TxtStyle.buttonText,))
           ],
         );
       });
 }
 static showErrorAlert(BuildContext context) async {
   await showDialog(context: (context),
       builder: (builder){
         return AlertDialog(
           icon: Icon(Icons.error_outline_sharp,color: Colors.red[200],size: 26,),
           title: Text('Error Occurred',style: TxtStyle.titleText,),
           content: Text('Oops...Something went wrong',textAlign:TextAlign.center,style: TxtStyle.bodyText,),
         );
       });
 }


}
