import 'package:flutter/material.dart';

class AppColors {
  static const primary = Colors.lightBlueAccent;
  static const primaryBlue = Colors.blueAccent;
  static  Color? secondary = Colors.grey[200];
  static Color? lightTextColor = Colors.grey[100];
  static Color darkTextColor = Colors.black87;
  static Color? textColorGreyDark = Colors.grey[700];
  static Color white = Colors.white;
  static Color? lightGrey = Colors.grey[300];
  static Color? blueGrey = Colors.blueGrey[800];
  static Color deepOrange = Colors.deepOrange;
  static Color deepOrangeAccent = Colors.deepOrangeAccent;
  static Color selectedOrange = Color(0xFFffcc99);
  static Color lightGreen = Colors.lightGreen;
  static Color pink = Colors.pink;

  static Color colorPrimary = Color(0xFF00bcd4);

  static Color PrimaryDark = Color(0xFF00bcd4);
  static Color colorAccent = Color(0xFF00bcd4);
  static Color image_border_start = Color(0x40990000);
  static Color image_border_end = Color(0xFF660000);
  static Color image_border_center = Color(0xFFFF3333);
  static Color colorPrimarylight = Color(0xFFD5EEF5);
  static Color red = Color(0xFFFF0000);
  static Color lesswhite = Color(0xFFC7C2C6);
  static Color green = Color(0xFF00FF00);

  static Color black = const Color(0xFF000000);
  static Color gray = const Color(0xFF646464);
  static Color navy = const Color(0xFF6699FF);
  static Color violet = const Color(0xFF7F00FF);
  static Color lightgray02 = const Color(0xFFbfbfbf);

  static Color darkorrange = Color(0xFFcc2303);
  static Color dark = Color(0xFF000015);
  static Color lightgreen = Color(0xFF336666);
  static Color white2 = Color(0xFFFFFFFF);
  static Color view_background = Color(0xe8ecfa);
  static Color btn_bg = Color(0xFF277bec);
  static Color txt_font = Color(0xFF4e5572);
  static Color alert = Color(0xFFA85E4F);

  static Color viewBg = Color(0xFFf1f5f8);
  static Color showCaseColor = Color(0x30000000);
  static Color heritage_color = Color(0xFFEDA84E);
  static Color light_grey = Color(0xFF888888);
  static Color pregnanacy_prob = Color(0xFFFAC090);
  static Color diariya = Color(0xFF00B050);
  static Color hipertantion = Color(0xFF376091);
  static Color swin_flue = Color(0xFF376091);
  static Color di_hydration = Color(0xFF92D050);
  static Color dyriya_with_dehydration = Color(0xFFC00000);
  static Color piliya = Color(0xFFFFC000);
  static Color maleriya = Color(0xFFE46D0A);
  static Color dengu = Color(0xFF974807);
  static Color tv = Color(0xFFFF0000);
  static Color mijals = Color(0xFFD99795);
  static Color chicken_pokes = Color(0xFF7030A0);
  static Color transparent = Color(0x00000000);
  static Color custom_header_text = Color(0xFF030303);
  static Color loginButton = Color(0xFF2AA7B8);


}