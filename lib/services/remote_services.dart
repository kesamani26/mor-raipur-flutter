import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';

import '../common/constants.dart';

class RemoteServices {

static Future<Map<String, dynamic>?> getDataModel(BuildContext context,String api,FormData formData) async {
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.contentType = 'multipart/form-data';
      Response response = formData.fields.isNotEmpty ?
      await dio.post(api, data: formData) : await dio.post(api);
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.data);
        return responseBody;
      } else {
        Constants.showErrorAlert(context,);
      }
    } catch (e) {
      // print(e);
    }
  }

  static Future<String?> putData(String api,FormData formData) async {
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.contentType = 'multipart/form-data';
      Response response = await dio.post(api, data: formData);

      if (response.statusCode == 200) {
        Map<String, dynamic> responseBody = response.data;
        // var common_response = CommonResponse.fromJson(responseBody);
        // return common_response.status;
      } else {
        // Constants.SuccessfulAlert('Something went wrong');
      }
    }
    catch (e) {
      // print(e);
    }
  }
}
