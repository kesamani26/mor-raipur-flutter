
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesClass{

  String user_id = 'user_id';
  String mobile = 'mobile';
  String profile_pic = 'profile_pic';
  String zone = 'zone';
  String ward = 'ward';
  String adhar_num = 'adhar_num';
  String email = 'email';
  String gender = 'gender';
  String first_name = 'first_name';
  String last_name = 'last_name';
  String UserName = 'UserName';
  String userTypeId = 'userTypeId';
  String userIdentityTypeId = 'userIdentityTypeId';
  String identityName = 'identityName';
  String isHealthOfficer = 'isHealthOfficer';
  String isAdmin = 'isAdmin';
  String ask_for_no_of_plant = 'ask_for_no_of_plant';

  String _register_imagepath = "register_imagepath";
  String _register_filename = "register_filename";
  String _register_compname = "register_compname";

  Future<bool> setuserId(String id) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(user_id, id);
  }

  Future<String?> getuserId() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(user_id);
  }

  Future<bool> setprofilePic(String pic) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(profile_pic, pic);
  }

  Future<String?> getprofilePic() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(profile_pic);
  }

  Future<bool> setzone(String z) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(zone, z);
  }

  Future<String?> getzone() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(zone);
  }

    Future<bool> setward(String w) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(ward, w);
  }

  Future<String?> getward() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(ward);
  }

  Future<bool> setadharNum(String adharN) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(adhar_num, adharN);
  }

  Future<String?> getadharNum() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(adhar_num);
  }

  Future<bool> setemail(String eml) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(email, eml);
  }

  Future<String?> getemail() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(email);
  }
  Future<bool> setfirstName(String name) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(first_name, name);
  }

  Future<String?> getfirstName() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(first_name);
  }

  Future<bool> setlastName(String name) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(last_name, name);
  }

  Future<String?> getlastName() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(last_name);
  }
  Future<bool> setuserName(String uname) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(UserName, uname);
  }

  Future<String?> getuserName() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(UserName);
  }

  Future<bool> setisAdmin(String admin) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(isAdmin, admin);
  }

  Future<String?> getisAdmin() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(isAdmin);
  }

  Future<String?> getgender() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(gender);
  }

  Future<bool> setgender(String gen) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(gender, gen);
  }

  Future<String?> getuserTypeId() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(userTypeId);
  }

  Future<bool> setuserTypeId(String utype) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(userTypeId, utype);
  }



  Future<bool> setregimagepath(String userId) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(_register_imagepath, userId);
  }

  Future<String?> getregimagepath() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(_register_imagepath);
  }

  Future<bool> setregfilename(String userId) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(_register_filename, userId);
  }

  Future<String?> getregfilename() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(_register_filename);
  }

  Future<bool> setregcompanyname(String userId) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.setString(_register_compname, userId);
  }

  Future<String?> getregcompanyname() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(_register_compname);
  }


  removeValues() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(user_id);
    sharedPreferences.remove(isAdmin);
    sharedPreferences.remove(gender);
  }

}