import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import '../common/colors.dart';
import '../style/txtStyle.dart';

class Grievance extends StatefulWidget {
  @override
  State<Grievance> createState() => _GrievanceState();
}

class _GrievanceState extends State<Grievance> {
  //grid view images rmc updates
  List<String> imagesgRID = [
    'assets/images/fire_pollution.png',
    'assets/images/solid_.png',
    'assets/images/water_.png',
    'assets/images/street_light_.png',
    'assets/images/encrochment_.png',
    'assets/images/gard_.png',
    'assets/images/stray_cattle_.png',
    'assets/images/hoarding_.png',
    'assets/images/road_re_.png',
  ];
  List<String> namesgRID = [
    'Air Pollution ',
    'Solid',
    'Water',
    'Street Light',
    'Encroachment',
    'Garden',
    'Stray Cattle',
    'Hoarding',
    'Road Repair',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Constants.buildAppBar(context,'grievances'),
      body: Container(
        width: Constants.deviceWidth(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 20, bottom: 10),
              child: Text(
                'GRIEVANCES',
                style: TxtStyle.subtitleGreyText,
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.all(6.0),
                child: GridView.builder(
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 3,
                      mainAxisExtent: 114,
                      // childAspectRatio: 4 / 2,
                      crossAxisSpacing: 0,
                      mainAxisSpacing: 0),
                  itemCount: 9,
                  itemBuilder: (context, index) {
                    return Column(
                      children: [
                        CircleAvatar(
                          radius: 35,
                          child:  Image.asset(imagesgRID[index],height: 50,width:50,),
                          backgroundColor: AppColors.viewBg,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 6.0),
                          child: Text(
                            namesgRID[index],
                            textAlign: TextAlign.center,
                            style: TxtStyle.bodyText,
                          ),
                        )
                      ],
                    );
                  },
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
