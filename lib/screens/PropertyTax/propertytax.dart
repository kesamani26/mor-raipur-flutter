import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import 'package:morraipur_flutter/screens/PropertyTax/paymentDetailPage.dart';
import 'package:morraipur_flutter/screens/PropertyTax/scannerPage.dart';

import 'package:morraipur_flutter/services/remote_services.dart';
import '../../common/colors.dart';
import '../../common/httpurl.dart';
import '../../models/OwnerName/SearchPropertyResult.dart';
import '../../models/colonyData/colony_namelist.dart';
import '../../models/propertyDetailmodel/getPropertyDetail.dart';
import '../../models/propertyTax/wardListModel.dart';
import '../../style/txtStyle.dart';
import '../../widgets_lib/widgetLib.dart';
import 'package:morraipur_flutter/style/customDailog.dart';

class PropertyTax extends StatefulWidget {
  const PropertyTax({Key? key}) : super(key: key);

  @override
  State<PropertyTax> createState() => _PropertyTaxState();
}

class _PropertyTaxState extends State<PropertyTax> {
  TextEditingController propertyId = TextEditingController();

  late WardListModel wardListModel;

  late ColonyNamelist colonyNamelist;
  late SearchPropertyResult searchPropertyResult;

  late Getpropdetailsresult getpropdetailsresult;
  var selectedWard = WardList(wardName: 'Select Ward', wardNo: '');
  var selectedColony = Data(
    colonyName: 'Select Ward',
  );
  var wardId;
  var colonyName;
  var propId;

  late Future<List<WardList>?> wardNolist;

  @override
  void initState() {
    getOwnerName();
    super.initState();
    getward();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Constants.appbar('Property Tax'),
      body: Stack(children: [
        SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 6.0),
                child: InkWell(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return const ScannerPage();
                        }),
                      ).then((error) {
                        if (error != null) {
                          customDailog.errorAlert(
                              context, 'Something went wrong');
                        }
                      });
                    },
                    child: Image.network(
                      'https://static.thenounproject.com/png/59262-200.png',
                      height: 70,
                      width: 70,
                    )),
              ),
              textWidget(),
              Padding(
                padding: const EdgeInsets.all(10),
                child: TextField(
                  controller: propertyId,
                  decoration: inputDecorations(
                    'Enter Property Id *',
                    'Enter Property Id',
                  ),
                  onChanged: (text) {
                    propId = text;
                  },
                ),
              ),
              textWidget(),
              wardNo(),
              colonydropDown(),
              ownerdropDown(),
              textWidget(),
              Padding(
                padding:
                    const EdgeInsets.symmetric(vertical: 6, horizontal: 10),
                child: TextField(
                  controller: propertyId,
                  decoration: inputDecorations(
                    'Mobile No',
                    'Enter Mobile No',
                  ),
                ),
              ),
              const SizedBox(
                height: 45,
              )
            ],
          ),
        ),
        Container(
          // bottom: 0,
          padding: const EdgeInsets.symmetric(horizontal: 2.0),
          alignment: Alignment.bottomCenter,
          child: Row(
            children: [
              const SizedBox(
                width: 35,
              ),
              Expanded(
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        backgroundColor: AppColors.colorPrimary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      child: Text(
                        'SEARCH',
                        style: TxtStyle.customText(
                            Colors.white, 16, FontWeight.w500),
                      ),
                      onPressed: () => {
                            if (propId != null ||
                                wardId != null && colonyName != null)
                              {getPropertyDetail()}
                            else
                              {
                                showGuideAlert(context),
                              }
                          })),
              const SizedBox(
                width: 12,
              ),
              Expanded(
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        backgroundColor: AppColors.colorPrimary,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      child: Text(
                        'CANCEL',
                        style: TxtStyle.customText(
                            Colors.white, 16, FontWeight.w500),
                      ),
                      onPressed: () => {
                            Navigator.pop(context),
                          })),
              const SizedBox(
                width: 35,
              ),
            ],
          ),
        ),
      ]),
    );
  }

  void showGuideAlert(BuildContext context) async {
    await showDialog(
        context: (context),
        builder: (builder) {
          return AlertDialog(
            title: Text(
              'Property Search Guide',
              style:
                  TxtStyle.customText(AppColors.txt_font, 20, FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            content: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                  'Searching Property tax details is now even easier.',
                  style: TxtStyle.customText(
                      AppColors.txt_font, 16, FontWeight.w500),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 4.0, bottom: 8),
                  child: Text(
                    'Follow any of the following steps.',
                    style: TxtStyle.customText(
                        AppColors.txt_font, 16, FontWeight.w500),
                  ),
                ),
                dynamicTextWidget(
                    '1. Scan QR-Code from Digital Door Number Plate'),
                smalltextWidget(),
                dynamicTextWidget('2. Enter PROPERTY_ID and hit search button'),
                smalltextWidget(),
                dynamicTextWidget(
                    '3. Enter mobile number and hit search button'),
                smalltextWidget(),
                dynamicTextWidget(
                    '4. Manually select ward-no , colony name and owner name'),
              ],
            ),
            actions: [
              Center(
                child: TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      'Close',
                      style: TxtStyle.customText(
                          AppColors.alert, 18, FontWeight.w400),
                    )),
              )
            ],
          );
        });
  }

  Text dynamicTextWidget(String text) {
    return Text(
      text,
      style: TxtStyle.customText(AppColors.txt_font, 15, FontWeight.w400),
    );
  }

  Padding smalltextWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Center(
          child: Text(
        'or',
        textAlign: TextAlign.center,
        style: TxtStyle.customText(AppColors.alert, 15, FontWeight.w400),
      )),
    );
  }

  Padding textWidget() {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 2.0),
      child: Text(
        'OR',
        style: TxtStyle.bigBrownText,
      ),
    );
  }

  Padding wardNo() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: DropdownSearch<WardList>(
        popupProps: PopupProps.dialog(
          dialogProps: DialogProps(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8))),
          showSearchBox: true,
          searchFieldProps: searchFieldProps('Search Here'),
          title: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Select Ward', style: TxtStyle.titleText),
              ),
              const Divider(
                height: 2,
                color: Colors.black45,
              )
            ],
          ),
        ),

        asyncItems: (String data) => getward(),

        dropdownDecoratorProps:
            dropDownDecoratorProps('Ward No. *', 'Search and Select Ward No.'),
        compareFn: (i, s) => false,
        itemAsString: (WardList gen) => gen.wardName,
        // popupTitle:
        onChanged: (value) {
          setState(() {
            wardId = value?.wardNo.toString();
            colonyName = '';
          });
        },
        validator: (value) {
          if (selectedWard.toString().length == 0) {
            return 'Please Select the Ward';
          }
          return null;
        },
        selectedItem: selectedWard,
      ),
    );
  }

  Padding colonydropDown() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: DropdownSearch<Data>(
        popupProps: PopupProps.dialog(
          dialogProps: DialogProps(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8))),
          showSearchBox: true,
          searchFieldProps: searchFieldProps('Search Here'),
          title: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Select Colony', style: TxtStyle.titleText),
              ),
              const Divider(
                height: 2,
                color: Colors.black45,
              )
            ],
          ),
        ),

        asyncItems: (String data) => getColony1(),
        dropdownDecoratorProps: dropDownDecoratorProps(
            'Colony Name *', 'Search and Select Colony Name '),
        compareFn: (i, s) => false,
        itemAsString: (Data gen) => gen.colonyName!,

        onChanged: (value) {
          setState(() {
            colonyName = value?.colonyName;
            print(colonyName);
          });
        },
        validator: (value) {
          if (colonyName.toString().length == 0) {
            return 'Please Select the Colony';
          }
          return null;
        },
        // selectedItem: colonyName,
      ),
    );
  }

  Padding ownerdropDown() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: DropdownSearch<Property>(
        popupProps: PopupProps.dialog(
          dialogProps: DialogProps(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8))),
          showSearchBox: true,
          searchFieldProps: searchFieldProps('Search Here'),
          title: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Select Owner', style: TxtStyle.titleText),
              ),
              const Divider(
                height: 2,
                color: Colors.black45,
              )
            ],
          ),
        ),

        asyncItems: (String data) => getOwnerName(),
        dropdownDecoratorProps: dropDownDecoratorProps(
            'Owner Name *', 'Search and Select Owner Name '),
        compareFn: (i, s) => false,
        itemAsString: (Property gen) => gen.ownerName!,
        // popupTitle:
        onChanged: (value) {
          setState(() {
            propId = value?.propertyUid.toString();
          });
          getPropertyDetail();
        },
        validator: (value) {
          if (value.toString().length == 0) {
            return 'Please Select the Owner';
          }
          return null;
        },
      ),
    );
  }

  Future<List<WardList>> getward() async {
    List<WardList> list = [];
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      var response = await RemoteServices.getDataModel(
          context, HttpUrl.get_Ward_Info, FormData());
      wardListModel = WardListModel.fromJson(response!);
      list = wardListModel.response;
    } catch (e) {
      print(e);
    }
    return list;
  }

  Future<List<Data>> getColony() async {
    List<Data> namelist = [];
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.responseType = ResponseType.plain;

      Response response = await dio.get(
          'https://apis.mcraipur.in/api/payment/locality/list?WARD_NO=$wardId');
      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.data);
        colonyNamelist = ColonyNamelist.fromJson(responseBody);
        namelist = colonyNamelist.data;
      } else {
        showErrorAlert(context, 'Network error occurred');
      }
    } catch (e) {
      print(e);
    }
    return namelist;
  }

  Future<List<Data>> getColony1() async {
    List<Data> namelist = [];
    List<Data> namelistnew = [];
    try {
      namelist = await getColony();
      namelistnew = namelist.where((element) {
        return element.colonyName != null;
      }).toList();
    } catch (e) {
      print(e);
    }
    return namelistnew;
  }

  Future<List<Property>> getOwnerName() async {
    List<Property> namelist = [];
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      Response response = await dio.get(
          'https://apis.mcraipur.in/api/searchPropertyList?WARD_NO=$wardId&COLONY_NAME=$colonyName');
      if (response.statusCode == 200) {
        final json = jsonDecode(response.data);
        searchPropertyResult = SearchPropertyResult.fromJson(json);
        namelist = searchPropertyResult.properties;
      } else {
        showErrorAlert(context, 'Network error occurred');
      }
    } catch (e) {
      print(e);
    }
    return namelist;
  }

  Future<dynamic> getPropertyDetail() async {
    progressDialog(context).show();
    List<Getpropdetai> propDetail = [];
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.contentType = 'application/json';
      dio.options.responseType = ResponseType.plain;
      Response response = await dio.get(
          'https://apis.mcraipur.in/api/getPropertyDetails?PROP_UID=$propId');

      if (response.statusCode == 200) {
        progressDialog(context).hide();
        try {
          final jsonString = response.data;

          // Remove extra string from the response
          final validJsonString = jsonString.substring(
            jsonString.indexOf('{'),
            jsonString.lastIndexOf('}') + 1,
          );

          // Decode the valid JSON string
          final jsonData = jsonDecode(validJsonString);

          getpropdetailsresult = Getpropdetailsresult.fromJson(jsonData);
          propDetail = getpropdetailsresult.getpropdetailsResult;

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => PaymentDetailPage(propDetail[0])));
        } catch (e) {
          print(e);
        }
      } else {
        showErrorAlert(context, 'Network error occurred');
      }
    } catch (e) {
      print(e);
    }
    progressDialog(context).hide();
  }
}
