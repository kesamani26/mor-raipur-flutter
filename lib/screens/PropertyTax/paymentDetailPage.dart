import 'package:flutter/material.dart';
import 'package:morraipur_flutter/style/txtStyle.dart';
import '../../common/colors.dart';
import '../../models/propertyDetailmodel/getPropertyDetail.dart';
import '../../widgets_lib/widgetLib.dart';

class PaymentDetailPage extends StatefulWidget {
  final Getpropdetai propDetail;
  const PaymentDetailPage(this.propDetail) ;

  @override
  State<PaymentDetailPage> createState() => _PaymentDetailPageState();
}

class _PaymentDetailPageState extends State<PaymentDetailPage> {
  late Getpropdetai propDetail;

  @override
  void initState() {
    propDetail = widget.propDetail;
  }
    @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Raipur Municipal Corporation',
          style: TxtStyle.titleText,
        ),
        backgroundColor: AppColors.colorPrimary,
        actions: [
          Image.asset('assets/images/mor_raipur_newlook.png'),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.only(left: 25.0, right: 25, top: 10),
        child: Stack(children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Text(
                  'Payment Details',
                  style: TxtStyle.brownText,
                ),
                const Divider(
                  thickness: 1,
                  // color: Colors.black,
                ),
                const SizedBox(
                  height: 13,
                ),
                Text(
                  widget.propDetail.propertyOwner ??'',
                  textAlign: TextAlign.center,
                  style: TxtStyle.headerText,
                ),
                Text(
                    widget.propDetail.address??'',
                    textAlign: TextAlign.center,
                    style: TxtStyle.titleText),
                const Divider(
                  thickness: 2,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Propery UID',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Relation',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Propery Type',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Ward Number',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Ward Name',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Locality Name',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'Total Cover Area',
                          style: TxtStyle.bodyGreyText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          'House Number',
                          style: TxtStyle.bodyGreyText,
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.propDetail.propUid??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                         widget.propDetail.fatherSonDauName ??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.propertyUse??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.wardNo.toString() ??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.wardName??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.colonyName??'',
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.totalCoveredArea.toString(),
                          style: TxtStyle.smallText,
                        ),
                        const SizedBox(
                          height: 6,
                        ),
                        Text(
                          widget.propDetail.houseNo??'-',
                          style: TxtStyle.smallText,
                        ),
                      ],
                    )
                  ],
                ),
                Text(
                  'Property Tax',
                  style: TxtStyle.bigBrownText,
                  textAlign: TextAlign.start,
                ),
                Container(
                  margin: const EdgeInsets.symmetric(vertical: 4),
                  decoration: BoxDecoration(
                      border: Border.all(color: AppColors.colorPrimary),
                      borderRadius: BorderRadius.circular(4)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Rebate',
                              style: TxtStyle.bodyGreyText,
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Text(
                              'Due Amount',
                              style: TxtStyle.bodyGreyText,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 38.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              widget.propDetail.rebate.toString(),
                              style: TxtStyle.bodyGreyText,
                            ),
                            const SizedBox(
                              height: 10,
                            ),

                            Text(
                              widget.propDetail.dueAmount.toString(),
                              style: TxtStyle.bodyGreyText,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 4.0),
                  child: Row(
                    children: [
                      Text(
                        'Total Payable Amount',
                        style: TxtStyle.titleText,
                      ),
                      const SizedBox(
                        width: 30,
                      ),
                      Text(
                        widget.propDetail.dueAmount.toString(),
                        style: TxtStyle.titleText,
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 6, horizontal: 1),
                  child: TextField(
                    decoration: inputDecorations(
                      'Mobile Number *',
                      'Enter Mobile Number',
                    ),
                  ),
                ),
                const SizedBox(
                  height: 5,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 6, horizontal: 1),
                  child: TextField(
                    decoration: inputDecorations(
                      'Email Id ',
                      'Enter Email Id',
                    ),
                  ),
                ),
                const SizedBox(
                  height: 45,
                ),
              ],
            ),
          ),
          Container(
            // bottom: 0,
            padding: const EdgeInsets.symmetric(horizontal: 2.0),
            alignment: Alignment.bottomCenter,
            child: Row(
              children: [
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                    child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: AppColors.colorPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            'Pay Via Payment Gateway',
                            style: TxtStyle.customText(
                                Colors.white, 15, FontWeight.w500),
                          ),
                        ),
                        onPressed: () => {
                              // Navigator.push(
                              //     context,
                              //     MaterialPageRoute(
                              //         builder: (context) =>
                              //             PaymentDetailPage())),
                              /*if (_formKey.currentState.validate()) {
                                current_dealId !=null ?
                                updateDeal():
                                submitDeal(),
                              }*/
                            })),
                const SizedBox(
                  width: 10,
                ),

              ],
            ),
          ),
        ]),
      ),
    );
  }
}



