import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import 'package:morraipur_flutter/common/httpurl.dart';
import 'package:morraipur_flutter/models/propertyDetailmodel/getPropertyDetail.dart';
import 'package:morraipur_flutter/screens/PropertyTax/paymentDetailPage.dart';
import 'package:morraipur_flutter/services/remote_services.dart';
import 'package:morraipur_flutter/style/customDailog.dart';
import 'package:morraipur_flutter/style/txtStyle.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:vibration/vibration.dart';


class ScannerPage extends StatefulWidget {
  const ScannerPage({Key? key}) : super(key: key);

  @override
  State<ScannerPage> createState() => _ScannerPageState();
}

class _ScannerPageState extends State<ScannerPage> with TickerProviderStateMixin  {

  late AnimationController animeController;
  QRViewController? qrController;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');

  Future<bool?> hasVibration() async {
    return await Vibration.hasVibrator();
  }

  @override
  void initState() {
    // animeController = AnimationController(
    //   /// [AnimationController]s can be created with `vsync: this` because of
    //   /// [TickerProviderStateMixin].
    //   vsync: this,
    //   duration: const Duration(seconds: 5),
    // )..addListener(() {
    //   setState(() {});
    // });
    // animeController.repeat(reverse: true);
    super.initState();
  }
  @override
  void dispose() {
    animeController.dispose();
    qrController?.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      qrController!.pauseCamera();
    } else if (Platform.isIOS) {
      qrController!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
        MediaQuery.of(context).size.height < 400)
        ? 200.0
        : 350.0;
    return Scaffold(
      appBar: Constants.appbar('Property QR Scan'),
      body: Column(
        children: [
          Flexible(
            flex: 8,
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                  borderColor: Colors.lightBlueAccent,
                  borderRadius: 6,
                  borderWidth: 4,
                  cutOutSize: scanArea),
            ),
          ),
          Flexible(
            flex: 1,
            child: Container(
              child: Center(child: Text('Scan QR',style: TxtStyle.titleText,)),
            ),
          )
        ],
      ),
    );
  }
  void onQrScanned() async {
    var barCode = await qrController?.scannedDataStream.first;
    bool? _hasVibration = await hasVibration();
    if (_hasVibration!) {
      Vibration.vibrate(duration: 100);
      showBottomBox(context);
      var ddn = barCode?.code?.substring(barCode.code!.lastIndexOf('=') + 1);
      var propertyId = await getPropertyIdFromDDN(ddn);
      getPropertyDetail(propertyId);
    }
  }
  void _onQRViewCreated(QRViewController qrController)  {
    this.qrController = qrController;
    onQrScanned();
  }
  void showBottomBox(BuildContext context){

    showModalBottomSheet(
        context: context,
        isDismissible: false,
        builder: (BuildContext context){

           return Container(
               height: 150,
               // width: 40,
               child: const Align(
                   alignment: Alignment.center,
                   child: CircularProgressIndicator(
                     // value: animeController.value,
                     // semanticsLabel: 'Circular progress indicator',
                   )));
        }
    );
  }

  Future<String?> getPropertyIdFromDDN(ddn) async {
    try {
      FormData formData = FormData.fromMap({
        'ddn' : ddn
      });
      var response = await RemoteServices.getDataModel(context, HttpUrl.get_property_id_from_ddn, formData);
      print('Respnse: $response');
      return response!['property_id'].toString();
    } catch (e) {
      print(e);
    }
  }

  Future<dynamic> getPropertyDetail(propId) async{
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      dio.options.contentType = 'application/json';
      dio.options.responseType = ResponseType.plain;
      // Response response = await dio.get('https://apis.mcraipur.in/api/getPropertyDetails?PROP_UID=$propId');
      Response response = await dio.get('https://apis.mcraipur.in/api/getPropertyDetails?PROP_UID=$propId');

      if(response.statusCode == 200){
        try {
          final jsonString = response.data;
          final validJsonString = jsonString.substring(
            jsonString.indexOf('{'),
            jsonString.lastIndexOf('}') + 1,
          );
          // Decode the valid JSON string
          final jsonData = jsonDecode(validJsonString);
          var getPropDetailsResult = Getpropdetailsresult.fromJson(jsonData);
          var propDetail = getPropDetailsResult.getpropdetailsResult;
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => PaymentDetailPage(propDetail[0])));

        } catch (e) {
          Navigator.pop(context,'error');
        }
      }else{
        Navigator.pop(context,'error');
      }
    } catch (e) {
      print(e);
    }
  }

}
