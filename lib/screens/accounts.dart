import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import '../common/colors.dart';
import '../shared_preference.dart';
import '../style/txtStyle.dart';
import 'login.dart';

class UserAccount extends StatefulWidget {
  const UserAccount({Key? key}) : super(key: key);

  @override
  State<UserAccount> createState() => _UserAccountState();
}

class _UserAccountState extends State<UserAccount> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 60,
        elevation: 0,
        title: Text(
          'account',
          style: TxtStyle.headerBlackText,
        ),
        backgroundColor: AppColors.viewBg,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(
            Icons.arrow_back_rounded,
            size: 30,
            color: Colors.black,
          ),
        ),
      ),
      body: Container(
        color: AppColors.viewBg,
        child: Column(
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
              child: Container(
                width: Constants.deviceWidth(context),
                child: Card(
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: Image.asset(
                            'assets/images/user_profile_newlook.png',
                            height: 75,
                            width: 75,
                          ),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              'Kesamani',
                              style: TxtStyle.headerText,
                            ),
                            const SizedBox(
                              height: 2,
                            ),
                            Text(
                              '6265228444',
                              style: TxtStyle.subtitleText,
                            ),
                          ],
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                      //set border radius more than 50% of height and width to make circle
                    )),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5),
              child: Container(
                width: Constants.deviceWidth(context),
                child: Card(
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        circularCard('my order',
                            'assets/images/receive_order_newlook.png'),
                        circularCard('my activity',
                            'assets/images/activity_newlook.png'),
                        circularCard(
                            'share app', 'assets/images/mobile_otp_.png'),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                      //set border radius more than 50% of height and width to make circle
                    )),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 10.0, vertical: 0),
              child: Container(
                width: Constants.deviceWidth(context),
                child: Card(
                    child: Column(
                      children: [
                        listTileView('assets/images/help_newlook.png', 'Support'),
                        deviderView(),
                        listTileView('assets/images/rate_app_newlook.png', 'Rate App'),
                        deviderView(),
                        listTileView('assets/images/about_newlook.png', 'About App'),
                        deviderView(),
                        listTileView('assets/images/logout_newlook.png', 'Logout'),
                       ],
                    ),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                      //set border radius more than 50% of height and width to make circle
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget circularCard(String name, String imgUrl) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: [
          Container(
            height: 60,
            width: 60,
            child: Card(
                elevation: 3,
                child: Image.asset(
                  imgUrl,
                  height: 30,
                  width: 30,
                ),
                color: AppColors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(95),
                  //set border radius more than 50% of height and width to make circle
                )),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 4.0),
            child: Text(
              name,
              textAlign: TextAlign.center,
              style: TxtStyle.bodyText,
            ),
          )
        ],
      ),
    );
  }

  Widget listTileView(String imgUrl, String name) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: ListTile(
        onTap: () async {
          if (name == 'Logout') {
            await SharedPreferencesClass().removeValues();
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Login()),
            );
          }
        },
        leading: CircleAvatar(
            backgroundColor:  AppColors.viewBg,
            radius: 40,
            child: Image.asset(
              imgUrl,
              height: 30,
              width: 30,
            ),
           ),
        title: Text(name,style: TxtStyle.subtitleText,),
        trailing: const Icon(Icons.arrow_forward_ios_outlined,size: 20,color: Colors.black54,),
      ),
    );
  }

  Widget deviderView() {
    return const Divider(
      endIndent: 10,
      indent: 10,
      thickness: 0.5,
      color: Colors.black26,
    );
  }
}
