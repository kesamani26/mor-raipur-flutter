import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import '../common/colors.dart';
import '../style/txtStyle.dart';

class Support extends StatefulWidget {
  @override
  State<Support> createState() => _SupportState();
}

class _SupportState extends State<Support> {
  //grid view images rmc updates
  List<String> imagesgRID = [
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/whatsapp.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/instagram.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/letter.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/sms.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/twitter.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/website.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/youtube.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/email.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/facebook.png',
    'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/nidan_newlook',

  ];
  List<String> namesgRID = [
    'Whatsapp ',
    'Instagram',
    'Write Us',
    'SMS',
    'Twitter',
    'Website',
    'YouTube ',
    'Email',
    'Facebook',
    'Toll Free Number',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Constants.buildAppBar(context,'support'),
      body: Container(
        color: AppColors.viewBg,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                mainAxisExtent: 115,
                childAspectRatio: 4 / 3,
                crossAxisSpacing: 8,
                mainAxisSpacing: 8),
            itemCount: 10,
            itemBuilder: (context, index) {
              return Card(
                elevation: 2,
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(child: Image.network((imagesgRID[index]),width: 45,
                    height: 45,)),
                    Center(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 17.0,left: 6,right: 6,top: 0),
                        child: Text(
                          namesgRID[index],
                          textAlign: TextAlign.center,
                          style: TxtStyle.bodyGreyText,
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
