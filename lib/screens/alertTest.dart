
import 'package:flutter/material.dart';
import 'package:morraipur_flutter/style/customDailog.dart';
import 'package:morraipur_flutter/widgets_lib/widgetLib.dart';

class AlertDauilogs extends StatefulWidget {
  AlertDauilogs({Key? key, this.title}) : super(key: key);

  final String? title;

  @override
  _AlertDauilogsState createState() => _AlertDauilogsState();
}

class _AlertDauilogsState extends State<AlertDauilogs> {
  @override
  Widget build(BuildContext context) {
    final successAlert = buildButton(
      onTap: () {
        customDailog.successAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Success',
      color: Colors.green,
    );

    final errorAlert = buildButton(
      onTap: () {
        customDailog.errorAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Error',
      color: Colors.red,
    );

    final warningAlert = buildButton(
      onTap: () {
        customDailog.warningAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Warning',
      color: Colors.orange,
    );

    final infoAlert = buildButton(
      onTap: () {
        customDailog.infoAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Info',
      color: Colors.blue[100],
    );

    final confirmAlert = buildButton(
      onTap: () {
        customDailog.confirmAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Confirm',
      color: Colors.lightGreen,
    );

    final loadingAlert = buildButton(
      onTap: () {
        customDailog.loadingAlert(BuildContext,);
      },
      text: 'Loading',
      color: Colors.grey,
    );

    final customAlert = buildButton(
      onTap: () {
        customDailog.customAlert(BuildContext,'Succesfully Completed');
      },
      text: 'Custom',
      color: Colors.orange,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title!),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            successAlert,
            errorAlert,
            warningAlert,
            infoAlert,
            confirmAlert,
            loadingAlert,
            customAlert,
          ],
        ),
      ),
    );
  }


}