import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import 'package:morraipur_flutter/screens/PropertyTax/propertytax.dart';
import '../common/colors.dart';
import '../common/httpurl.dart';
import '../style/txtStyle.dart';

class PayOnline extends StatefulWidget {
  @override
  State<PayOnline> createState() => _PayOnlineState();
}

class _PayOnlineState extends State<PayOnline> {
  // online service list images
  List<String> images = [
    'bpms_newlook.png',
    'water_connection.png',
    'namantran.png',
  ];
  List<String> names = [
    'BPMS',
    'Water',
    'Namantaran',
    'More',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Constants.buildAppBar(context,'pay online'),
      body: Container(
        color: AppColors.viewBg,
        child: Padding(
          padding: const EdgeInsets.all(2.0),
          child: Column(
            children: [
              Card(
                child: Container(
                  height: 200,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 15.0, top: 10),
                        child: Text(
                          'Online Services',
                          style: TxtStyle.subtitleGreyText,
                        ),
                      ),
                      Flexible(
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 3,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 15.0,horizontal: 5),
                              child: Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    GestureDetector(
                                      onTap : (){
                                        Navigator.push(context, MaterialPageRoute(builder: (context) =>PropertyTax()));
                                      },
                                      child: Container(
                                             child: Padding(
                                            padding: const EdgeInsets.all(18.0),
                                            child:Image.network(HttpUrl.HttpHost_For_Icon+images[index],height: 60,width: 60,),
                                          )),
                                    ),
                                    Center(
                                      child: Padding(
                                        padding: const EdgeInsets.only(bottom: 8.0),
                                        child: Text(
                                          names[index],
                                          textAlign: TextAlign.center,
                                          style: TxtStyle.bodyGreyText,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                 )
              ),
            ],
          ),
        ),
      ),
    );
  }
}
