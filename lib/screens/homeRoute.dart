import 'package:flutter/material.dart';
import 'package:morraipur_flutter/screens/pay.dart';
import 'grievances.dart';
import 'help.dart';
import 'home.dart';



class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectedIndex = 0;
  late List<Widget> actions;
  final List<Widget> _pages = <Widget>[
    Home(),
    PayOnline(),
    Grievance(),
    Support(),
   
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       body: Center(
          child: _pages.elementAt(_selectedIndex), //New
        ),
      bottomNavigationBar: BottomNavigationBar(
          selectedItemColor: Colors.black,
          showUnselectedLabels: true,
          unselectedItemColor: Colors.grey[500],
          backgroundColor: Colors.blueGrey[50],
          iconSize: 27,
          elevation: 1,
          currentIndex: _selectedIndex,
          onTap: (int index) {
            if (!mounted) return;
            setState(() {
              _selectedIndex = index;
            });
          },
          items: [
            const BottomNavigationBarItem(icon: Icon(Icons.home_outlined), label: 'Home'),
            const BottomNavigationBarItem(icon: Icon(Icons.credit_card), label: 'Pay'),
            const BottomNavigationBarItem(icon: Icon(Icons.gpp_bad), label: 'Grievances'),
            const BottomNavigationBarItem(icon: Icon(Icons.help_center_outlined), label: 'Help'),

          ]),
    );
  }

  
}






