import 'package:flutter/material.dart';
import 'package:morraipur_flutter/common/constants.dart';
import 'package:morraipur_flutter/models/appImagesModal/app_images_modal.dart';
import 'package:morraipur_flutter/style/txtStyle.dart';
import 'package:carousel_slider/carousel_slider.dart';
import '../common/colors.dart';
import '../common/httpurl.dart';
import '../widgets_lib/widgetLib.dart';

//slider images
final List<String> imgList = [
  'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/rmc_scl',
  'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/rmc_pani',
  'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/rmc_trust',
  'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/rmc_parking',
  'http://nagarnigamprojects.in/morraipur/websiervice/webservice/AppImages/Medieval_Three.png',

];

class Home extends StatefulWidget {
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  // online service list images
  List<String> images = [
    'bpms_newlook.png',
    'water_connection.png',
    'namantran.png',
    'more_newlook.png',

  ];
  List<String> names = [
    'BPMS',
    'Water',
    'Namantaran',
    'More',
  ];

  //grid view images rmc updates
  List<String> imagesgRID = [
    'upcoming_activity_newlook',
    'gallery_newlook',
    'about_newlook',
    'track_myward_newlook',
    'fire_station_newlook',
    'emergency_phone_newlook',

  ];
  List<String> namesgRID = [
    'Upcoming Activities',
    'Gallery',
    'About',
    'Track My Ward',
    'Fire Station',
    'Important Nubmers',
  ];

  //for raipurian images of images
  List<String> imagList = [
    'assets/images/air_quality_newlook.png',
    'assets/images/event.png',
    'assets/images/plant_tree.png',
  ];

  List<String> bottomImage = [
    'assets/images/heritage.png',
    'assets/images/tourist_places.png',
    'assets/images/eateris_newlook.png',
  ];

  List<String> name = [
    'Air Quality',
    'Event In City',
    'Plant Tree',
  ];

  int _current = 0;
  final CarouselController _controller = CarouselController();
  late AppImagesModal appImagesModal;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: Constants.buildAppBar(context,'mor raipur'),
      body: Container(
        color: AppColors.viewBg,
        child: SingleChildScrollView(
          child: Column(
            children: [
              onlineServiceCard(),
              sliderImages(),
              rmcUpdatesCard(),
              nearMeServiceCard(),
              forRaipurian(),
              bottomList(),
            ],
          ),
        ),
      ),
    );
  }

  Padding onlineServiceCard() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: Constants.deviceWidth(context),
        child: Card(
          elevation: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 10),
                  child: Text(
                    'Online Services',
                    style: TxtStyle.subtitleGreyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 10.0, bottom: 4, top: 4),
                  child: Container(
                    height: 100,
                    width: Constants.deviceWidth(context),
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: 4,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              CircleAvatar(
                                radius: 30,
                                child: Image.network(HttpUrl.HttpHost_For_Icon+images[index],height: 40,width: 40,),
                                // backgroundImage: Image.network(HttpUrl.HttpHost_For_Icon+images[index],),
                                backgroundColor: AppColors.viewBg,
                              ),
                              Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: Text(
                                  names[index],
                                  style: TxtStyle.smallBodyText,
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              //set border radius more than 50% of height and width to make circle
            )),
      ),
    );
  }

  Padding sliderImages() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8, vertical: 2),
      child: Stack(
        children: [
          InkWell(
            onTap: () {},
            child: CarouselSlider(
              items: imgList
                  .map((item) => Card(
                        elevation: 2,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            item,
                            fit: BoxFit.cover,
                            width: double.infinity,
                          ),
                        ),
                      ))
                  .toList(),
              carouselController: _controller,
              options: CarouselOptions(
                  autoPlay: true,
                  enlargeCenterPage: true,
                  aspectRatio: 2.0,
                  scrollPhysics: const BouncingScrollPhysics(),
                  viewportFraction: 1,
                  onPageChanged: (index, reason) {
                    if (!mounted) return;
                    setState(() {
                      _current = index;
                    });
                  }),
            ),
          ),
          Positioned(
            bottom: 10,
            left: 0,
            right: 0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: imgList.asMap().entries.map((entry) {
                return GestureDetector(
                  onTap: () => _controller.animateToPage(entry.key),
                  child: Container(
                    width: 6.0,
                    height: 6.0,
                    margin: const EdgeInsets.symmetric(
                        vertical: 4.0, horizontal: 4.0),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: (Theme.of(context).brightness == Brightness.dark
                                ? Colors.white
                                : Colors.black)
                            .withOpacity(_current == entry.key ? 0.9 : 0.4)),
                  ),
                );
              }).toList(),
            ),
          )
        ],
      ),
    );
  }

  Padding rmcUpdatesCard() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Container(
        width: Constants.deviceWidth(context),
        child: Card(
          elevation: 2,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 15.0, top: 10),
                  child: Text(
                    'RMC Updates',
                    style: TxtStyle.subtitleGreyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 2.0, bottom: 4, top: 14),
                  child: Container(
                    height: 210,
                    width: Constants.deviceWidth(context),
                    child: GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 4,
                              mainAxisExtent: 110,
                              childAspectRatio: 4 / 3,
                              crossAxisSpacing: 0,
                              mainAxisSpacing: 0),
                      itemCount: 6,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.all(2.0),
                          child: Column(
                            children: [
                              CircleAvatar(
                                radius: 30,
                                child: Image.network(HttpUrl.HttpHost_For_Icon+imagesgRID[index],height: 40,width: 40,),
                                // backgroundImage: AssetImage(imagesgRID[index]),
                                backgroundColor: AppColors.viewBg,
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.symmetric(vertical: 8.0),
                                child: Text(
                                  namesgRID[index],
                                  textAlign: TextAlign.center,
                                  style: TxtStyle.smallBodyText,
                                ),
                              )
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                )
              ],
            ),
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              //set border radius more than 50% of height and width to make circle
            )),
      ),
    );
  }

  Padding nearMeServiceCard() {
    return Padding(
      padding: EdgeInsets.all(8),
      child: Container(
        width: Constants.deviceWidth(context),
        child: Card(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 15),
                  child: Text(
                    'Services',
                    style: TxtStyle.subtitleGreyText,
                  ),
                ),
                Center(
                  child: Container(
                      height: 90,
                      width: 90,
                      child: const Image(
                          image: AssetImage('assets/images/near_me.png'))),
                ),
                Center(
                    child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 20.0),
                  child: Text(
                    'NEAR ME',
                    style: TxtStyle.bodyText,
                  ),
                )),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: buildBrownButton(context,
                      text: 'Click', color: Colors.grey[700], onTap: () {}),
                ),
              ],
            ),
            color: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
              //set border radius more than 50% of height and width to make circle
            )),
      ),
    );
  }

  Widget forRaipurian() {
    return Card(
      elevation: 5,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(40), topRight: Radius.circular(40)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 20),
            child: Text(
              'For Raipurian',
              style: TxtStyle.subtitleGreyText,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10.0, bottom: 4, top: 4),
            child: Container(
              height: 270,
              width: Constants.deviceWidth(context),
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: 3,
                itemBuilder: (context, index) {
                  return Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15.0, horizontal: 4),
                    child: Container(
                      width: 160,
                      child: Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        color: name[index] == 'Air Quality'
                            ? Colors.lightBlue[50]
                            : name[index] == 'Plant Tree'
                                ? Colors.lime[200]
                                : Colors.cyan[200],
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Image.asset(imagList[index],
                                  width: 100, height: 100, fit: BoxFit.fill),
                              Padding(
                                padding: const EdgeInsets.only(top: 6.0),
                                child: Text(
                                  name[index],
                                  style: TxtStyle.bodyText,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    top: 8.0, left: 6, right: 6),
                                child: buildBrownButton(context,
                                    text: 'Explore Now',
                                    color: Colors.grey[700],
                                    onTap: () {}),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget bottomList() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Card(
        elevation: 0,
        child: Container(
          height: 140,
          width: Constants.deviceWidth(context),
          child: ListView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.horizontal,
            itemCount: 3,
            itemBuilder: (context, index) {
              return Padding(
                padding: const EdgeInsets.only(right: 2,left: 2.0,top: 15),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),

                  child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: Image.asset(bottomImage[index], fit: BoxFit.fill,width: 200,)),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}
