import 'package:dio/dio.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:morraipur_flutter/common/constants.dart';
import 'package:pin_code_fields/pin_code_fields.dart';

import '../common/colors.dart';
import '../common/httpurl.dart';
import '../models/loginDetail.dart';
import '../shared_preference.dart';
import '../style/txtStyle.dart';
import '../widgets_lib/widgetLib.dart';
import 'homeRoute.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  List<String> userType = [
    'INDIVIDUAL',
    'GOVERNMENT',
    'NGO',
    'PRIVATE COMPANY',
    'GREEN ARMY',
    'VENDER'
  ];
  String? selecteduserType = '';
  var mobileCntrl;
  TextEditingController nameCntrl = TextEditingController(text: '');
  String currentvalue_otp = '', mobile_number = '';
  late LoginDetail loginDetail;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding: MediaQuery.of(context).viewInsets,
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            color: AppColors.viewBg,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 60,
                ),
                Image.asset(
                  'assets/images/logo_newlook.png',
                  width: 210,
                ),
                const SizedBox(height: 20),
                Text(
                  'raipur \n municipal corporation',
                  textAlign: TextAlign.center,
                  style: TxtStyle.titleGreyText,
                ),
                const SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Card(
                    elevation: 3,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 32, left: 15, right: 15),
                          child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                            ],
                            decoration: inputDecoration(
                                'Mobile Number',
                                'Enter Mobile Number',
                                const Icon(
                                  Icons.mobile_friendly,
                                  color: Colors.black54,
                                )),
                            onChanged: (text) {
                              mobileCntrl = text;
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 32, left: 15, right: 15),
                          child: TextField(
                            controller: nameCntrl,
                            decoration: inputDecoration(
                                'Name',
                                'User Name',
                                const Icon(
                                  Icons.people,
                                  color: Colors.black54,
                                )),
                          ),
                        ),
                        userTypedropDown(),
                        const SizedBox(
                          height: 15,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 42.0, vertical: 5),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            child: OutlinedButton(
                                style: OutlinedButton.styleFrom(
                                  backgroundColor: AppColors.loginButton,
                                  shape: RoundedRectangleBorder(
                                    borderRadius:
                                        BorderRadius.circular(12), // <-- Radius
                                  ),
                                ),
                                onPressed: () {
                                  _modalBottomSheetMenu();
                                  if (mobileCntrl == null) {
                                    showErrorAlert(context,  'Please Enter Mobile Number');
                                  } else if (mobileCntrl.length != 10) {
                                    showErrorAlert(context, 'Mobile No. Must Be 10 Digit');
                                  } else if (nameCntrl.text.isEmpty) {
                                    showErrorAlert(context,'Please Enter User Name');
                                  } else {
                                    userLogin();
                                  }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.all(11.0),
                                  child: Text(
                                    'Login',
                                    style: TxtStyle.buttonTextstyle,
                                  ),
                                )),
                          ),
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Padding userTypedropDown() {
    return Padding(
      padding: EdgeInsets.only(top: 32, left: 15, right: 15),
      child: DropdownSearch<dynamic>(
          items: userType,
          popupProps: const PopupProps.menu(fit: FlexFit.loose),
          selectedItem: selecteduserType,
          dropdownDecoratorProps:
              dropDownDecoratorProps('Who are you --', 'Select User Type'),
          onChanged: (value) {
            selecteduserType = value.toString();
          },
          autoValidateMode: AutovalidateMode.onUserInteraction,
          validator: (value) {
            if (value == null) {
              return 'Please select User Type';
            }
            return null;
          }),
    );
  }

  Future<void> userLogin() async {
    progressDialog(context).show();
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      FormData formData;
      formData = FormData.fromMap({
        'username': nameCntrl.text,
        'mobileno': mobileCntrl,
        'user_type': '1',
        'user_identity_type': '',
        'user_identity_name': '',
      });
      Response response =
          await dio.post(HttpUrl.login_with_otp, data: formData);
      if (response.statusCode == 200) {
        Map<String, dynamic> responseBody = response.data;
        if (responseBody['status'] == 'true') {
          progressDialog(context).hide();
          _modalBottomSheetMenu();
          loginDetail = LoginDetail.fromJson(responseBody);
        }
      } else {
        showErrorAlert(context, 'Something Went Wrong');
      }

      print(response.toString());
    } catch (e) {
      print(e);
    }
    progressDialog(context).hide();
  }


  Future<void> validate_otp() async {
    progressDialog(context).show();
    try {
      Dio dio = Dio();
      dio.options.connectTimeout = 50000;
      FormData formData;
      formData = FormData.fromMap({
        'otp': currentvalue_otp,
        'mobileno': mobileCntrl,
        'id': loginDetail.userId,
      });
      Response response = await dio.post(HttpUrl.verify_Registration_OTP, data: formData);

        Map<String, dynamic> responseBody = response.data;
        if (response.statusCode == 200) {
          if (responseBody['status'] == 'true') {
            progressDialog(context).hide();
            print(loginDetail.name);
            await SharedPreferencesClass().setuserId(loginDetail.userId!);
            await SharedPreferencesClass().setfirstName(loginDetail.firstName!);
            await SharedPreferencesClass().setadharNum(loginDetail.adharNum!);
            await SharedPreferencesClass().setemail(loginDetail.email!);
            await SharedPreferencesClass().setgender(loginDetail.gender!);
            await SharedPreferencesClass().setisAdmin(loginDetail.isAdmin!);
            await SharedPreferencesClass().setprofilePic(loginDetail.profilePic!);
            await SharedPreferencesClass().setuserName(loginDetail.name!);
            await SharedPreferencesClass().setward(loginDetail.ward!);
            await SharedPreferencesClass().setzone(loginDetail.zone!);

            /*Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => Login()),
            );*/
            //succesfull login ka toast dikhana hai
           Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
          } else {
            /*Fluttertoast.showToast(
              msg: responseBody['message'],
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
            );*/
          }
        } else {
         /* Fluttertoast.showToast(
            msg: 'Please Try After Some Time',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
          );*/
        }

    } catch (e) {
      print(e);
    }

  }


  void _modalBottomSheetMenu() {
    showModalBottomSheet(
        enableDrag: false,
        isScrollControlled: true,
        isDismissible: false,
        context: context,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            top: Radius.circular(35.0),
          ),
        ),
        builder: (builder) {
          return Padding(
            padding: MediaQuery.of(context).viewInsets,
            child: new Container(
              height: 350.0,
              color: Colors.transparent,

              child: Column(

                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 42.0),
                      child: Text(
                        'Verification',
                        style: TxtStyle.bigheaderText,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 14),
                      child: Text(
                        'verify mobile number please enter the recieve otp $mobile_number',
                        style: TxtStyle.subtitleGreyText,
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 30.0, horizontal: 30),
                        child: PinCodeTextField(
                          appContext: context,
                          pastedTextStyle: const TextStyle(
                            color: Colors.black54,
                            fontWeight: FontWeight.bold,
                          ),
                          length: 4,
                          blinkWhenObscuring: true,
                          animationType: AnimationType.fade,

                          pinTheme: PinTheme(
                            shape: PinCodeFieldShape.box,
                            borderRadius: BorderRadius.circular(2),
                            fieldHeight: 55,
                            fieldWidth: 55,
                            activeFillColor: Colors.white,
                            inactiveFillColor: Colors.white,
                            errorBorderColor : Colors.black54,
                            selectedFillColor: Colors.white,
                            borderWidth: 0.5,
                          ),


                          enableActiveFill: false,

                          keyboardType: TextInputType.number,

                          onCompleted: (v) {
                            debugPrint('Completed');
                          },
                          onChanged: (value) {
                            debugPrint(value);
                            if (value.length == 4) {
                            setState(() {
                              currentvalue_otp = value;
                            });
                          }},
                          beforeTextPaste: (text) {
                            debugPrint("Allowing to paste $text");
                            //if you return true then it will show the paste confirmation dialog. Otherwise if false, then nothing will happen.
                            //but you can show anything you want here, like your pop up saying wrong paste format or etc
                            return true;
                          },
                        )),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 42.0, vertical: 5),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: OutlinedButton(
                            style: OutlinedButton.styleFrom(
                              backgroundColor: AppColors.loginButton,
                              shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.circular(12), // <-- Radius
                              ),
                            ),
                            onPressed: () {
                              validate_otp();
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(11.0),
                              child: Text(
                                'Sign In',
                                style: TxtStyle.buttonTextstyle,
                              ),
                            )),
                      ),
                    ),
                  ]),
            ),
          );
        });
  }


}
