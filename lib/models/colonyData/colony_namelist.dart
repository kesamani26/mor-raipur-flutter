import 'package:json_annotation/json_annotation.dart';

part 'colony_namelist.g.dart';

@JsonSerializable()
class ColonyNamelist {
  final int? status;
  final String? message;
  final List<Data> data;

  const ColonyNamelist({
    this.status,
    this.message,
    required this.data,
  });

  factory ColonyNamelist.fromJson(Map<String, dynamic> json) =>
      _$ColonyNamelistFromJson(json);

  Map<String, dynamic> toJson() => _$ColonyNamelistToJson(this);
}

@JsonSerializable()
class Data {
  @JsonKey(name: 'COLONY_NAME')
  final String? colonyName;

  const Data({
    this.colonyName,
  });

  factory Data.fromJson(Map<String, dynamic> json) {
    try {
      return _$DataFromJson(json);
    } catch (e) {
      return const Data();
    }
  }


  Map<String, dynamic> toJson() => _$DataToJson(this);
}
