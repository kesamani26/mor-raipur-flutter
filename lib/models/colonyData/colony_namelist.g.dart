// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'colony_namelist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ColonyNamelist _$ColonyNamelistFromJson(Map<String, dynamic> json) =>
    ColonyNamelist(
      status: json['status'] as int?,
      message: json['message'] as String?,
      data: (json['data'] as List<dynamic>)
          .map((e) => Data.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ColonyNamelistToJson(ColonyNamelist instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data,
    };

Data _$DataFromJson(Map<String, dynamic> json) => Data(
      colonyName: json['COLONY_NAME'] as String?,
    );

Map<String, dynamic> _$DataToJson(Data instance) => <String, dynamic>{
      'COLONY_NAME': instance.colonyName,
    };
