
import 'dart:convert';

LoginDetail loginDetailFromJson(String str) => LoginDetail.fromJson(json.decode(str));

String loginDetailToJson(LoginDetail data) => json.encode(data.toJson());

class LoginDetail {
  LoginDetail({
      this.status,
     this.isHealthOfficer,
     this.userId,
     this.otp,
     this.name,
     this.firstName,
     this.lastName,
     this.gender,
     this.zone,
     this.ward,
     this.adharNum,
     this.profilePic,
     this.email,
     this.userTypeId,
     this.userIdentityType,
     this.userIdentityName,
     this.isAdmin,
     this.askForNoOfPlant,
     this.message,
  });

  String? status;
  String? isHealthOfficer;
  String? userId;
  String? otp;
  String? name;
  String? firstName;
  String? lastName;
  String? gender;
  String? zone;
  String? ward;
  String? adharNum;
  String? profilePic;
  String? email;
  String? userTypeId;
  String? userIdentityType;
  String? userIdentityName;
  String? isAdmin;
  String? askForNoOfPlant;
  String? message;

  factory LoginDetail.fromJson(Map<String, dynamic> json) => LoginDetail(
    status: json["status"],
    isHealthOfficer: json['isHealthOfficer'],
    userId: json['user_id'],
    otp: json['otp'],
    name: json['name'],
    firstName: json['first_name'],
    lastName: json['last_name'],
    gender: json['gender'],
    zone: json['zone'],
    ward: json['ward'],
    adharNum: json['adhar_num'],
    profilePic: json['profile_pic'],
    email: json['email'],
    userTypeId: json['user_type_id'],
    userIdentityType: json['user_identity_type'],
    userIdentityName: json['user_identity_name'],
    isAdmin: json['isAdmin'],
    askForNoOfPlant: json['ask_for_no_of_plant'],
    message: json['message'],
  );

  Map<String, dynamic> toJson() => {
    'status': status,
    'isHealthOfficer': isHealthOfficer,
    'user_id': userId,
    'otp': otp,
    'name': name,
    'first_name': firstName,
    'last_name': lastName,
    'gender': gender,
    'zone': zone,
    'ward': ward,
    'adhar_num': adharNum,
    'profile_pic': profilePic,
    'email': email,
    'user_type_id': userTypeId,
    'user_identity_type': userIdentityType,
    'user_identity_name': userIdentityName,
    'isAdmin': isAdmin,
    'ask_for_no_of_plant': askForNoOfPlant,
    'message': message,
  };
}
