// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'facilities_data_modal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FacilitiesDataModal _$FacilitiesDataModalFromJson(Map<String, dynamic> json) =>
    FacilitiesDataModal(
      status: json['status'] as bool?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => Response.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$FacilitiesDataModalToJson(
        FacilitiesDataModal instance) =>
    <String, dynamic>{
      'status': instance.status,
      'response': instance.response,
    };

Response _$ResponseFromJson(Map<String, dynamic> json) => Response(
      id: json['id'] as String?,
      catName: json['cat_name'] as String?,
      icon: json['icon'] as String?,
    );

Map<String, dynamic> _$ResponseToJson(Response instance) => <String, dynamic>{
      'id': instance.id,
      'cat_name': instance.catName,
      'icon': instance.icon,
    };
