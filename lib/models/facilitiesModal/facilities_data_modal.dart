import 'package:json_annotation/json_annotation.dart';

part 'facilities_data_modal.g.dart';

@JsonSerializable()
class FacilitiesDataModal {
  final bool? status;
  final List<Response>? response;

  const FacilitiesDataModal({
    this.status,
    this.response,
  });

  factory FacilitiesDataModal.fromJson(Map<String, dynamic> json) =>
      _$FacilitiesDataModalFromJson(json);

  Map<String, dynamic> toJson() => _$FacilitiesDataModalToJson(this);
}

@JsonSerializable()
class Response {
  final String? id;
  @JsonKey(name: 'cat_name')
  final String? catName;
  final String? icon;

  const Response({
    this.id,
    this.catName,
    this.icon,
  });

  factory Response.fromJson(Map<String, dynamic> json) =>
      _$ResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseToJson(this);
}
