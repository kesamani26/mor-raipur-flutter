import 'package:json_annotation/json_annotation.dart';

part 'app_images_modal.g.dart';

@JsonSerializable()
class AppImagesModal {
  final String? status;
  final List<Response>? response;

  const AppImagesModal({
    this.status,
    this.response,
  });

  factory AppImagesModal.fromJson(Map<String, dynamic> json) =>
      _$AppImagesModalFromJson(json);

  Map<String, dynamic> toJson() => _$AppImagesModalToJson(this);
}

@JsonSerializable()
class Response {
  final String? file;
  final String? ImageId;
  final String? ImageCode;
  final String? InsentiveTitle;
  final String? InsentiveMatter;
  @JsonKey(name: 'link_url')
  final String? linkUrl;

  const Response({
    this.file,
    this.ImageId,
    this.ImageCode,
    this.InsentiveTitle,
    this.InsentiveMatter,
    this.linkUrl,
  });

  factory Response.fromJson(Map<String, dynamic> json) =>
      _$ResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseToJson(this);
}
