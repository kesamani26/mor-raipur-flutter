// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_images_modal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppImagesModal _$AppImagesModalFromJson(Map<String, dynamic> json) =>
    AppImagesModal(
      status: json['status'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => Response.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$AppImagesModalToJson(AppImagesModal instance) =>
    <String, dynamic>{
      'status': instance.status,
      'response': instance.response,
    };

Response _$ResponseFromJson(Map<String, dynamic> json) => Response(
      file: json['file'] as String?,
      ImageId: json['ImageId'] as String?,
      ImageCode: json['ImageCode'] as String?,
      InsentiveTitle: json['InsentiveTitle'] as String?,
      InsentiveMatter: json['InsentiveMatter'] as String?,
      linkUrl: json['link_url'] as String?,
    );

Map<String, dynamic> _$ResponseToJson(Response instance) => <String, dynamic>{
      'file': instance.file,
      'ImageId': instance.ImageId,
      'ImageCode': instance.ImageCode,
      'InsentiveTitle': instance.InsentiveTitle,
      'InsentiveMatter': instance.InsentiveMatter,
      'link_url': instance.linkUrl,
    };
