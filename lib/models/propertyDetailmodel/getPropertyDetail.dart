

import 'dart:convert';

Getpropdetailsresult getpropdetailsresultFromJson(String str) => Getpropdetailsresult.fromJson(json.decode(str));

String getpropdetailsresultToJson(Getpropdetailsresult data) => json.encode(data.toJson());

class Getpropdetailsresult {
  Getpropdetailsresult({
    required this.status,
    required this.message,
    required this.getpropdetailsResult,
  });

  int status;
  String message;
  List<Getpropdetai> getpropdetailsResult;

  factory Getpropdetailsresult.fromJson(Map<String, dynamic> json) => Getpropdetailsresult(
    status: json['status'],
    message: json['message'],
    getpropdetailsResult: List<Getpropdetai>.from(json['GETPROPDETAILSResult'].map((x) => Getpropdetai.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    'status': status,
    'message': message,
    'GETPROPDETAILSResult': List<dynamic>.from(getpropdetailsResult.map((x) => x.toJson())),
  };
}

class Getpropdetai {
  Getpropdetai({
    required this.propUid,
    required this.wardNo,
    required this.wardName,
    required this.houseNo,
    required this.colonyName,
    required this.propertyOwner,
    required this.fatherSonDauName,
    required this.address,
    required this.totalPlotArea,
    required this.totalCoveredArea,
    required this.zoneNo,
    required this.totalFloors,
    required this.propertyUse,
    required this.propertyTax,
    required this.rebate,
    required this.depositedTax,
    required this.dueAmount,
  });

  String propUid;
  int wardNo;
  String wardName;
  String houseNo;
  String colonyName;
  String propertyOwner;
  String fatherSonDauName;
  String address;
  int totalPlotArea;
  int totalCoveredArea;
  String zoneNo;
  int totalFloors;
  String propertyUse;
  int propertyTax;
  int rebate;
  String depositedTax;
  int dueAmount;

  factory Getpropdetai.fromJson(Map<String, dynamic> json) => Getpropdetai(
    propUid: json['PROP_UID'],
    wardNo: json['WARD_NO'],
    wardName: json['WARD_NAME'],
    houseNo: json['HOUSE_NO'],
    colonyName: json['COLONY_NAME'],
    propertyOwner: json['PROPERTY_OWNER'],
    fatherSonDauName: json['FATHER_SON_DAU_NAME'],
    address: json['ADDRESS'],
    totalPlotArea: json['TOTAL_PLOT_AREA'],
    totalCoveredArea: json['TOTAL_COVERED_AREA'],
    zoneNo: json['ZONE_NO'],
    totalFloors: json['TOTAL_FLOORS'],
    propertyUse: json['PROPERTY_USE'],
    propertyTax: json['PROPERTY_TAX'],
    rebate: json['REBATE'],
    depositedTax: json['DEPOSITED_TAX'],
    dueAmount: json['DUE_AMOUNT'],
  );

  Map<String, dynamic> toJson() => {
    'PROP_UID': propUid,
    'WARD_NO': wardNo,
    'WARD_NAME': wardName,
    'HOUSE_NO': houseNo,
    'COLONY_NAME': colonyName,
    'PROPERTY_OWNER': propertyOwner,
    'FATHER_SON_DAU_NAME': fatherSonDauName,
    'ADDRESS': address,
    'TOTAL_PLOT_AREA': totalPlotArea,
    'TOTAL_COVERED_AREA': totalCoveredArea,
    'ZONE_NO': zoneNo,
    'TOTAL_FLOORS': totalFloors,
    'PROPERTY_USE': propertyUse,
    'PROPERTY_TAX': propertyTax,
    'REBATE': rebate,
    'DEPOSITED_TAX': depositedTax,
    'DUE_AMOUNT': dueAmount,
  };
}
