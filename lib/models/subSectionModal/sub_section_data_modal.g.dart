// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_section_data_modal.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SubSectionDataModal _$SubSectionDataModalFromJson(Map<String, dynamic> json) =>
    SubSectionDataModal(
      status: json['status'] as String?,
      message: json['message'] as String?,
      response: (json['response'] as List<dynamic>?)
          ?.map((e) => Response.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SubSectionDataModalToJson(
        SubSectionDataModal instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'response': instance.response,
    };

Response _$ResponseFromJson(Map<String, dynamic> json) => Response(
      ssid: json['ssid'] as String?,
      ssname: json['ssname'] as String?,
      sid: json['sid'] as String?,
    );

Map<String, dynamic> _$ResponseToJson(Response instance) => <String, dynamic>{
      'ssid': instance.ssid,
      'ssname': instance.ssname,
      'sid': instance.sid,
    };
