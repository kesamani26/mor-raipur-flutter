import 'package:json_annotation/json_annotation.dart';

part 'sub_section_data_modal.g.dart';

@JsonSerializable()
class SubSectionDataModal {
  final String? status;
  final String? message;
  final List<Response>? response;

  const SubSectionDataModal({
    this.status,
    this.message,
    this.response,
  });

  factory SubSectionDataModal.fromJson(Map<String, dynamic> json) =>
      _$SubSectionDataModalFromJson(json);

  Map<String, dynamic> toJson() => _$SubSectionDataModalToJson(this);
}

@JsonSerializable()
class Response {
  final String? ssid;
  final String? ssname;
  final String? sid;

  const Response({
    this.ssid,
    this.ssname,
    this.sid,
  });

  factory Response.fromJson(Map<String, dynamic> json) =>
      _$ResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ResponseToJson(this);
}
