
import 'dart:convert';

WardListModel wardListModelFromJson(String str) => WardListModel.fromJson(json.decode(str));

String wardListModelToJson(WardListModel data) => json.encode(data.toJson());

class WardListModel {
  WardListModel({
    required this.status,
    required this.response,
  });

  bool status;
  List<WardList> response;

  factory WardListModel.fromJson(Map<String, dynamic> json) => WardListModel(
    status: json['status'],
    response: List<WardList>.from(json['response'].map((x) => WardList.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    'status': status,
    'response': List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class WardList {
  WardList({
    required this.wardNo,
    required this.wardName,
  });

  String wardNo;
  String wardName;

  factory WardList.fromJson(Map<String, dynamic> json) => WardList(
    wardNo: json['ward_no'],
    wardName: json['ward_name'],
  );

  Map<String, dynamic> toJson() => {
    'ward_no': wardNo,
    'ward_name': wardName,
  };


}
