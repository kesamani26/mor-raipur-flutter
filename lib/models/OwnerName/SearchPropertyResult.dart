class SearchPropertyResult {

  String? message;
  List<Property> properties;

  SearchPropertyResult({ this.message, required this.properties});

  factory SearchPropertyResult.fromJson(Map<String, dynamic> json) {
    var searchPropertyResultList = json['SearchPropertyResult'] as List;
    List<Property> propertiesList =
    searchPropertyResultList.map((i) => Property.fromJson(i)).toList();
    return SearchPropertyResult(

      message: json['message'],
      properties: propertiesList,
    );
  }
}

class Property {
  String? propertyUid;
  dynamic? ownerName;
  dynamic? wardNo;
  dynamic? locality;
  dynamic? address;
  // String? fatherSonDaughter;
  // dynamic? totalPlotArea;
  // int? totalFloors;

  Property({
     this.propertyUid,
     this.ownerName,
     this.wardNo,
     this.locality,
     this.address,
    // required this.fatherSonDaughter,
    // required this.totalPlotArea,
    // required this.totalFloors,
  });

  factory Property.fromJson(Map<String, dynamic> json) {
    return Property(
      propertyUid: json['PROPERTY_UID'],
      ownerName: json['OWNER_NAME'],
      wardNo: json['WARD_NO'],
      locality: json['LOCALITY'],
      address: json['ADDRESS'],
      // fatherSonDaughter: json['FATHER/SON/DAUGHTER'],
      // totalPlotArea: json['TOTAL_PLOT_AREA'] !=null ? int.parse(json['TOTAL_PLOT_AREA']) : 0,
      // totalFloors: json['TOTAL_FLOORS'] ?? 0,
    );
  }
}
